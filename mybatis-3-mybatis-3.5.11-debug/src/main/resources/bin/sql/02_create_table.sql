
-- run this script with user mybatis.

use mybatis ;

CREATE TABLE `emp`
(
    `id`       bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
    `emp_no`   varchar(20)  DEFAULT '',
    `emp_name` varchar(100) DEFAULT '',
    PRIMARY KEY (`id`)
) ;

insert into emp (emp_no, emp_name) values('001','zhangsan') ;
insert into emp (emp_no, emp_name) values('002','lisi') ;
