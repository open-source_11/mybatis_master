
-- run this script in mysql with root.

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mybatis` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

create user `mybatis`@`localhost` identified by 'mybatis@2022' ;
grant all privileges on mybatis.* to `mybatis`@`localhost` ;

create user `mybatis`@`%` identified by 'mybatis@2022' ;
grant all privileges on mybatis.* to `mybatis`@`%` ;

