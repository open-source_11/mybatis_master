package org.messi.a_jdbc;

import org.messi.a_jdbc.entity.Emp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class JdbcWithParamsTest {

    public static void main(String[] args) throws Exception {
        // 1.连接驱动类
        Class.forName("com.mysql.cj.jdbc.Driver");
        // 2.获取连接
        String username = "mybatis";
        String password = "mybatis@2022";
        String url = "jdbc:mysql://127.0.0.1:3306/mybatis?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai";
        Connection conn = DriverManager.getConnection(url,username,password);

        // 3.准备发送SQL
        String sql = "select * from emp where id = ? and emp_name = ?";
        PreparedStatement pstm = conn.prepareStatement(sql);

        // MESSI 增加参数
        pstm.setString(1, "002");
        pstm.setString(2, "lisi2");

        // 4.发送执行SQL -- 直接使用mysql的驱动包，查询出数据
        ResultSet result = pstm.executeQuery();

        List<Emp> emps = new ArrayList<>() ;

        while(result.next()) {
//            System.out.println("通过index获取字段值");
//            System.out.println(result.getString(1));
//            System.out.println(result.getString(2));
//            System.out.println(result.getString(3));

//            System.out.println("通过字段名获取字段值");
//            System.out.println(result.getString("id"));
//            System.out.println(result.getString("emp_no"));
//            System.out.println(result.getString("emp_name"));

            Emp emp = new Emp() ;
            emp.setId( result.getInt("id") );
            emp.setEmpNo( result.getString("emp_no") );
            emp.setEmpName( result.getString("emp_name") );

            emps.add(emp) ;
        }

        System.out.println(emps);

        pstm.close();
        conn.close();
    }
}
