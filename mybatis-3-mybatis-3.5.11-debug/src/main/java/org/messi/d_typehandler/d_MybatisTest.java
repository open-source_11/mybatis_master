package org.messi.d_typehandler;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.messi.b_orm.entity.Emp;
import org.messi.c_mybatis.daowithoutxml.DaoWithoutXml;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * mybatis中使用到两级缓存
 */
public class d_MybatisTest {

    public static void main(String[] args) {
        String resource = "mybatis-config.xml" ;
        InputStream inputStream = null ;

        try{
            inputStream = Resources.getResourceAsStream(resource) ;
        } catch( IOException e )
        {
            e.printStackTrace();
        }

        // build 方法里会对所有配置文件进行解析
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream, "mybatis") ;
            // 创建了 DefaultSqlSessionFactory 对象
            // 解析出了实体、dao、xml文件。
            // 可以说，准备好了一切配置，就等执行了。

        SqlSession sqlSession = sqlSessionFactory.openSession();

        DaoWithoutXml mapper = getMapper(sqlSession, DaoWithoutXml.class) ;

        List<Emp> list = mapper.list("00", "name","s", new RowBounds(1,2));
        System.out.println(list);

        Emp emp = new Emp() ;
        emp.setId(2);
        emp = mapper.findEmpById(emp) ;

//        emp.setEmpNo("01");
//        emp.setEmpName("messi");
//        emp.setId(4);
//        mapper.insert2(emp) ;

//        EmpDao empMapper ;
//
//        // 获取数据库会话
//        // try块退出时，会自动调用 sqlSession.close() 方法，关闭资源。括号里可以写多个资源定义，每一个都会自动调用close方法。
//        try {
//            SqlSession sqlSession = sqlSessionFactory.openSession();
//            empMapper = getMapper(sqlSession, EmpDao.class) ;
//            Emp emp = empMapper.findEmpByEmpno("001");
//            System.out.println(emp.getEmpNo());
//            System.out.println(emp.getEmpName());
//
////            Emp emp2 = empMapper.findEmpByEmpname("lisi");
////            System.out.println(emp2.getEmpNo());
////            System.out.println(emp2.getEmpName());
//
//            Emp emp3 = empMapper.findEmpByEmpno("001");
//            System.out.println(emp3.getEmpNo());
//            System.out.println(emp3.getEmpName());
//
//            sqlSession.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try (SqlSession otherSqlSession = sqlSessionFactory.openSession()) {
//            empMapper = getMapper(otherSqlSession, EmpDao.class) ;
//            Emp emp = empMapper.findEmpByEmpno("001");
//
//            System.out.println(emp.getEmpNo());
//            System.out.println(emp.getEmpName());
//
//            Emp emp2 = empMapper.findEmpByEmpname("lisi");
//
//            System.out.println(emp2.getEmpNo());
//            System.out.println(emp2.getEmpName());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
    }

    private static <E> E getMapper(SqlSession sqlSession, Class<E> clazz){
        return (E) sqlSession.getMapper(clazz);
    }
}
