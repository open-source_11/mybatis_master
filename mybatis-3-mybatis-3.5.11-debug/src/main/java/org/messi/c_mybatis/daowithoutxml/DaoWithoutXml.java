package org.messi.c_mybatis.daowithoutxml;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.JdbcType;
import org.messi.b_orm.entity.Emp;
import org.messi.z_util.Nouse;

import java.util.List;

public interface DaoWithoutXml {

    @Results(id="empList", value = {
            @Result(column = "id", property = "id", jdbcType = JdbcType.TINYINT, id = true),
            @Result(column = "emp_no", property = "empNo"),
            @Result(column = "emp_name", property = "empName")
    })
    @Select("select id, emp_no, emp_name from emp where emp_no like concat('%', #{keyWord}, '%') and #{name}=#{name}")
    List<Emp> list(@Param("keyWord") String keyWord, @Param("name") String name, String s, RowBounds rb) ;

    @ResultMap("empList")
    @Select("select id, emp_no, emp_name from emp where id = #{id}")
    Emp findEmpById(Emp emp) ;

//    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
//    @Insert("insert into emp (id, emp_no, emp_name) values (#{id}, #{empNo}, #{empName})")
//    int insert(Emp emp) ;
//
//    @SelectKey(statement = "select id", keyProperty = "id", before = false, resultType = int.class)
//    @Insert("insert into emp (id, emp_no, emp_name) values (#{id}, #{empNo}, ${empName}, ${db.name}, ${input})")
//        // 在 sql 里，可以使用 ${xxx} 的方式，引入变量。
//    int insert2(Emp emp) ;
//
//    @Nouse
//    @Delete(databaseId = "oracle", value = "trancate table xxx")
//    @Delete("delete from emp where id=${id}")
//    void delete(@Param("id") int id);
}
