package org.messi.c_mybatis.daowithprovider;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.messi.b_orm.entity.Emp;


public interface DaoSelectProvider {
    @SelectProvider(type = SqlProvider.class, method = "selectById")
    Emp selectById(int id);

    public static class SqlProvider {
        public static String selectById() {
            return "SELECT id, name FROM users WHERE id = #{id}";
        }
    }
}
