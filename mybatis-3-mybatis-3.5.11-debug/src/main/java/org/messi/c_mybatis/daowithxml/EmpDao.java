package org.messi.c_mybatis.daowithxml;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.messi.b_orm.entity.Emp;

import java.util.List;

public interface EmpDao {

    int insert(Emp emp) ;

    Emp findEmpByEmpno(String empno) ;

    Emp findEmpByEmpname(String empname) ;

    List<Emp> getAllEmp() ;
}
