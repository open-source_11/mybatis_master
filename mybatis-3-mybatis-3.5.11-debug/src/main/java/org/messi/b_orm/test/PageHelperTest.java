package org.messi.b_orm.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.messi.b_orm.entity.Emp;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class PageHelperTest {

    public static void main(String[] args) {
        String resource = "mybatis-config.xml" ;
        InputStream inputStream = null ;

        try{
            inputStream = Resources.getResourceAsStream(resource) ;
        } catch( IOException e )
        {
            e.printStackTrace();
        }

        // build 方法里会对所有配置文件进行解析
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream, "mybatis") ;
            // 创建了 DefaultSqlSessionFactory 对象
            // 解析出了实体、dao、xml文件。
            // 可以说，准备好了一切配置，就等执行了。

        // 获取数据库会话
        // try块退出时，会自动调用 sqlSession.close() 方法，关闭资源。括号里可以写多个资源定义，每一个都会自动调用close方法。
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            List<Emp> emps = sqlSession.selectList("org.messi.b_orm.dao.EmpDao.getAllEmp", null, new RowBounds(0, 10));
            System.out.println("分页查询结束");
            System.out.println(emps);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
