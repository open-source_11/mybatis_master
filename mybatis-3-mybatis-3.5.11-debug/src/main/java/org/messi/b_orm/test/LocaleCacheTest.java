package org.messi.b_orm.test;

import java.io.IOException;
import java.io.InputStream;
import org.apache.ibatis.io.Resources ;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.messi.b_orm.dao.EmpDao;
import org.messi.b_orm.dao.StudentDao;
import org.messi.b_orm.dao.TeacherDao;
import org.messi.b_orm.entity.Emp;

/**
 * mybatis中使用到两级缓存
 */
public class LocaleCacheTest {

    public static void main(String[] args) {
        String resource = "mybatis-config.xml" ;
        InputStream inputStream = null ;

        try{
            inputStream = Resources.getResourceAsStream(resource) ;
        } catch( IOException e )
        {
            e.printStackTrace();
        }

        // build 方法里会对所有配置文件进行解析
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream, "mybatis") ;
            // 创建了 DefaultSqlSessionFactory 对象
            // 解析出了实体、dao、xml文件。
            // 可以说，准备好了一切配置，就等执行了。

        EmpDao empMapper ;
        StudentDao studentMapper ;
        TeacherDao teacherDao ;

        // 获取数据库会话
        // try块退出时，会自动调用 sqlSession.close() 方法，关闭资源。括号里可以写多个资源定义，每一个都会自动调用close方法。
        try {
            SqlSession sqlSession = sqlSessionFactory.openSession();
            empMapper = getMapper(sqlSession, EmpDao.class) ;
            Emp emp = empMapper.findEmpByEmpno("001");
            System.out.println(emp.getEmpNo());
            System.out.println(emp.getEmpName());

//            Emp emp2 = empMapper.findEmpByEmpname("lisi");
//            System.out.println(emp2.getEmpNo());
//            System.out.println(emp2.getEmpName());

            Emp emp3 = empMapper.findEmpByEmpno("001");
            System.out.println(emp3.getEmpNo());
            System.out.println(emp3.getEmpName());

            studentMapper = getMapper(sqlSession, StudentDao.class) ;
            emp3 = studentMapper.findEmpByEmpno("001");
            System.out.println(emp3.getEmpNo());
            System.out.println(emp3.getEmpName());

            sqlSession.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try (SqlSession otherSqlSession = sqlSessionFactory.openSession()) {
            empMapper = getMapper(otherSqlSession, EmpDao.class) ;
            Emp emp = empMapper.findEmpByEmpno("001");

            System.out.println(emp.getEmpNo());
            System.out.println(emp.getEmpName());

            Emp emp2 = empMapper.findEmpByEmpname("lisi");

            System.out.println(emp2.getEmpNo());
            System.out.println(emp2.getEmpName());

            studentMapper = getMapper(otherSqlSession, StudentDao.class) ;
            System.out.println(studentMapper);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static <E> E getMapper(SqlSession sqlSession, Class<E> clazz){
        return (E) sqlSession.getMapper(clazz);
    }
}
