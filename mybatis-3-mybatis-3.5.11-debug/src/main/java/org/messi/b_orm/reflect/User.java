package org.messi.b_orm.reflect;

import java.util.HashMap;
import java.util.Map;

public class User {

    private Map<String,String> userMap = new HashMap<>() ;

    public Map<String, String> getUserMap() {
        return userMap;
    }

    public void setUserMap(Map<String, String> userMap) {
        this.userMap = userMap;
    }
}
