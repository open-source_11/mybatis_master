package org.messi.b_orm.reflect;

import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

// 核心接口类
public class TestMain {
    public static void main(String[] args) {
        User user = new User();
        MetaObject metaObject = SystemMetaObject.forObject(user) ;

        metaObject.setValue("userMap[zhangsan]", "1234");

        System.out.println(metaObject.getValue("userMap[zhangsan]"));

        System.out.println((String)(user.getUserMap().get("zhangsan")));
    }

}
