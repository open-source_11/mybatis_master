package org.messi.b_orm.plugin.result;

import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.plugin.*;

import java.util.List;
import java.util.Properties;

@Intercepts({
        @Signature(type = ResultSetHandler.class, method = "handleResultSets", args = {java.sql.Statement.class})
})
public class MyResultSetHandlerPlugin implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        final List<Object> results = (List<Object>)invocation.proceed();
// 在这就可以处理字典
        for (Object result : results) {
            System.out.println("MyResultSetHandlerPlugin拦截器---拦截到结果" + result);
            System.out.println("MyResultSetHandlerPlugin拦截器---可以这里对结果进行处理");
        }
        return results ;
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
        System.out.println("获取到插件属性配置");
        Interceptor.super.setProperties(properties);
        System.out.println(properties.keys());
    }
}
