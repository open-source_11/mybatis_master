package org.messi.b_orm;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.messi.b_orm.dao.EmpDao;
import org.messi.b_orm.entity.Emp;

import java.util.List;

public class MybatisOrmTest {

    public static void main(String[] args) throws Exception {

        // 配置文件
        String resource = "mybatis-config-orm.xml" ;

        // 明确使用xml配置文件中 environments 节点的对应配置
        String environment = "mybatis" ;

        // build 方法里会对所有配置文件进行解析
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream(resource), environment) ;

        SqlSession sqlSession = sqlSessionFactory.openSession();

        EmpDao mapper = getMapper(sqlSession, EmpDao.class) ;

        // 无参查询
        List<Emp> list = mapper.getAllEmp();
        System.out.println(list);

        // 有参查询
        Emp emp = mapper.findEmpByEmpname("lisi") ;
        System.out.println(emp);

    }

    private static <E> E getMapper(SqlSession sqlSession, Class<E> clazz){
        return (E) sqlSession.getMapper(clazz);
    }
}
