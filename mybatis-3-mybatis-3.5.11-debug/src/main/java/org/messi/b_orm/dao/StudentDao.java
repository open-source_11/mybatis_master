package org.messi.b_orm.dao;

import org.messi.b_orm.entity.Emp;
import org.messi.b_orm.entity.Student;

public interface StudentDao {
    Student getStudent() ;
    Emp findEmpByEmpno(String empno) ;
}
