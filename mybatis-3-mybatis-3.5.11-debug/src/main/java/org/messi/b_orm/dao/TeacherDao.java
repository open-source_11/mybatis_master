package org.messi.b_orm.dao;

import org.messi.b_orm.entity.Teacher;

public interface TeacherDao {
    Teacher getTeacher() ;
}
