package org.messi.b_orm.dao;

import org.messi.b_orm.entity.Emp;

import java.util.List;

public interface EmpDao {

    int insert(Emp emp) ;

    Emp findEmpByEmpno(String empno) ;

    Emp findEmpByEmpname(String empname) ;

    List<Emp> getAllEmp() ;
}
