源码技术点
====


![mybatis](https://mybatis.org/images/mybatis-logo.png)

MyBatis源码中的重点的技术点

-----
[MyBatis配置详解](https://mybatis.net.cn/configuration.html)

#TODO
导入mybatis-srping的代码
在pom.xml中引入spring相关的包，写几个spring的controller，
调试代码，看mybatis在什么时候创建sqlSesstionFactory，什么时候创建sqlSession
可以在spring-framework工程中，加入mybatis的源代码，直接在spring-framework的工程中进行整合。
双数据源如何实现
插件如何实现（以分页插件为例）

Dao对象是如何导入到spring中去的？
MapperScannerConfigurer 类，是一个bfpp。在spring的invokeBeanFactoryPostProcessors方法中，
调用到了该类中的方法，加载了所有的Mapper对象。

mybatis插件 (https://blog.csdn.net/jason559/article/details/120368131)

#辅助知识点
##JDBC
一个简单的JDBC应用程序。

[PreparedStatement防止SQL注入原理。](https://www.cnblogs.com/roostinghawk/p/9703806.html)

##隔离级别
##动态代理
* CGLIB动态代理
* JDK动态代理

#源码解析
##重要的类
* MappedStatement
* Configuration
* MapperProxy
* LruCache

    最近最少使用的缓存
* FifoCache

##缓存
* 一级缓存
* 二级缓存  
cacheEnabled参数控制  
select标签中，useCache属性控制

##参数配置
* 延迟加载
* 缓存开关

##动态代理类的创建


##执行器
###执行器种类
###执行器包装

##拦截器
###拦截器种类
###拦截器链调用

##插件支持原理
###分页插件
###xxx插件

##标签
###cache-ref

#MyBatis扩展
##分页插件
##字典自动填充

#springboot整合
在与springboot整合之后，sqlsessionfactory什么时候创建？sqlsession什么时候创建？

#设计模式
* 工厂模式  
*不同的工厂，生产出的对象，是不一样的，但是却是同一接口的实例对象。*
*使用不同的工厂，实现的不同的实例*
* 装饰者模式
*mybatis中的cache实现，LruCache、FifoCache，就使用了装饰者模式*
*被装饰的对象，并不知道自己被装饰了。装饰之后的对象，却拥有了更多的功能*




